# MIR Experiments

Repository containing some experiments for my undergrad project.

## Objective
Understand if rhythm game beatmaps are based upon musical features, and, if so, which features.

### Ground Truth Used
Uses beatmaps from the game [osu!](https://osu.ppy.sh/home) downloaded from [bloodcat](https://bloodcat.com/osu).

Parses note onsets from these beatmaps using the [osu-parser NodeJS library](https://github.com/nojhamster/osu-parser)

### Musical Features Tested
Uses code from the [FMP Library](https://www.audiolabs-erlangen.de/resources/MIR/FMP/C0/C0.html) for processing audio and extracting `beats` and `onsets`

Tuned parameters are the `novelty_function` (`energy`, `spectral`, `phase`, `complex`)
used, `hop_size` (256, 512, 1024) and `window_length` (1024, 2048, 4096).

### Evaluation Method
Uses the `onset_evaluation` functions from the [MIR Evaluation Library](https://craffel.github.io/mir_eval/) for evaluating onset detection techniques.


## Running the Experiments

### Getting Started
Download beatmaps and install `node` dependencies running:
```
make setup
```
You must have `NodeJS` and the `yarn` package manager installed for this to work.

### Parsing Beatmaps
Run the following command:
```
make parse
```
This will create several new files based on the beatmaps downloaded earlier. For a beatmap with id `001`, for instance:
* `audio/001.mp3`: audio file of the song `001`
* `osu/001.osu`: xml file with all the beatmap information
* `notes/001.txt`: list of note onsets for beatmap `001`, processed from the `.osu` file

### Running Jupyter
Run the following command:
```
make jupyter
```
You must have `jupyter-notebook` and `librosa` installed for this to work.

## References

* Meinard Müller: Fundamentals of Music Processing. Springer Verlag, 2015.
* Meinard Müller and Frank Zalkow: FMP Notebooks: Educational Material for Teaching and Learning Fundamentals of Music Processing. Proceedings of the International Conference on Music Information Retrieval (ISMIR), Delft, The Netherlands, 2019. 
* Raffel, B. McFee, E. J. Humphrey, J. Salamon, O. Nieto, D. Liang, and D. P. W. Ellis, “mir_eval: A Transparent Implementation of Common MIR Metrics”, Proceedings of the 15th International Conference on Music Information Retrieval, 2014.