const path = require('path')
const util = require('util')
const fs = require('fs')
const readdir = util.promisify(fs.readdir)
const StreamZip = require('node-stream-zip')
const parser = require('osu-parser')

const folder = '../experiment-files/'
const directoryPath = path.join(__dirname, folder, 'osu-files')

const audioFolder = '../audio'
const notesFolder = '../notes'
const osuFolder = '../osu'

async function main() {
  processOsu('hello-easy', 'EASY.osu')
  
  // const files = await readdir(directoryPath)
  // files.map(async (file) => {
  //   if (file.indexOf('.osz') > -1) {
  //     try {
  //       const filepath = path.join(directoryPath, file)
  //       const zip = await extractFromOsz(filepath)
  //       if (zip) zip.close()
  //     } catch (err) {
  //       console.log(`Error in file ${file}: ${err}`)
  //     }
  //   }
  // })
}

function processOsu(id, osuFile) {
  parser.parseFile(osuFile, (err, beatmap) => {
    if (err) {
      console.log(`Error parsing ${id}: ${err}`)
      return
    }
    const noteTimes = beatmap.hitObjects.map((obj)=> parseInt(obj.startTime))
    const sortedNotes = noteTimes.sort((a,b) => a-b)
    fs.writeFileSync(path.join(directoryPath, notesFolder, `${id}.txt`), sortedNotes.join(','), 'utf-8')
  })
}

async function extractFromOsz(filepath) {
  const id = filepath.split('/').slice(-1).pop().split('.')[0]
  console.log(id)

  const zip = new StreamZip({
    file: filepath,
    storeEntries: true
  })

  try {
    zip.on('ready', () => {
      for (const entry of Object.values(zip.entries())) {
        if (entry.name.toLowerCase().indexOf('.mp3') > -1) {
          const filepath = path.join(directoryPath, audioFolder, `${id}.mp3`)
          zip.extract(entry.name, filepath, err => {
            if (err) console.log(`Extract error ${err}`)
          })
        }
        else if (entry.name.indexOf('.osu') > -1) {
          const filepath = path.join(directoryPath, osuFolder, `${id}.osu`)
          zip.extract(entry.name, filepath, err => {
            if (err) console.log(`Extract error ${err}`)
            try {
              processOsu(id, filepath)
            } catch {
              return zip
            }
          })
        }
      }
      return zip
    })
  } catch (err) {
    console.log(`Erro em ${id}: ${err}`)
    return zip
  }
}

main()